#include<iostream>

//include glad before GLFW to avoid header conflict or define "#define GLFW_INCLUDE_NONE"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


#include "camera.h"
#include "shader.h"
#include "object.h"

const int width = 1800;
const int height = 1000;


GLuint compileShader(std::string shaderCode, GLenum shaderType);
GLuint compileProgram(GLuint vertexShader, GLuint fragmentShader);
void processInput(GLFWwindow* window);


#ifndef NDEBUG
void APIENTRY glDebugOutput(GLenum source,
	GLenum type,
	unsigned int id,
	GLenum severity,
	GLsizei length,
	const char* message,
	const void* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}
#endif

Camera camera(glm::vec3(2.0, 2.0, 2.1));


int main(int argc, char* argv[])
{
	std::cout << "Welcome to this Project: " << std::endl;
	std::cout << "This project aims to create a small environement with lights and mirrors to play with lights and rendering \n";
	//TODO: Make some of the lights moove?
	//TODO: Add real player with its texture
	//TODO: (Optionnal) Make it so the player can move and the camera is behind him? 

	//TODO: Create a mirror with frame buffer
	//TODO: Compute shadows? 
	//TODO: Make particule effects (fog)?


	//Boilerplate
	//Create the OpenGL context 
	if (!glfwInit()) {
		throw std::runtime_error("Failed to initialise GLFW \n");
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifndef NDEBUG
	//create a debug context to help with Debugging
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif


	//Create the window
	GLFWwindow* window = glfwCreateWindow(width, height, "Exercise 07", nullptr, nullptr);
	if (window == NULL)
	{
		glfwTerminate();
		throw std::runtime_error("Failed to create GLFW window\n");
	}

	glfwMakeContextCurrent(window);
	
	//load openGL function
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		throw std::runtime_error("Failed to initialize GLAD");
	}

	glEnable(GL_DEPTH_TEST);

#ifndef NDEBUG
	int flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
#endif

	char fileVert[128] = PATH_TO_SHADERS "/player_vert.vert";
	char fileFrag[128] = PATH_TO_SHADERS "/base.frag";
	Shader player_shader(fileVert, fileFrag);

	char mapVert[128] = PATH_TO_SHADERS "/map_vert.vert";
	char mapFrag[128] = PATH_TO_SHADERS "/map_frag.frag";
	Shader map_shader(mapVert, mapFrag);

	char lVert[128] = PATH_TO_SHADERS "/light.vert";
	char lFrag[128] = PATH_TO_SHADERS "/light.frag";
	Shader l_shader(lVert, lFrag);

	char skyVert[128] = PATH_TO_SHADERS "/skybox.vert";
	char skyFrag[128] = PATH_TO_SHADERS "/skybox.frag";
	Shader sky_shader(skyVert, skyFrag);

	

	char path_player[] = PATH_TO_OBJECTS "/kriegsman.obj";
	Object player(path_player);
	player.makeObject(player_shader, false);

	char path_plane[] = PATH_TO_OBJECTS "/desertmap.obj";
	Object plane(path_plane);
	plane.makeObject(map_shader, true);

	char path_light[] = PATH_TO_OBJECTS "/sphere_smooth.obj";
	Object light1(path_light);
	light1.makeObject(l_shader, false);
	Object light2(path_light);
	light2.makeObject(l_shader, false);
	Object light3(path_light);
	light3.makeObject(l_shader, false);
	Object light4(path_light);
	light4.makeObject(l_shader, false);

	char path_skybox[] = PATH_TO_OBJECTS "/skybox.obj";
	Object sky(path_skybox);
	sky.makeObject(sky_shader, true);

	GLuint texture_map;
	glGenTextures(1, &texture_map);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_map);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


	stbi_set_flip_vertically_on_load(true);
	int imWidth_map, imHeight_map, imNrChannels_map;
	char file_map[128] = "../../../../LAB03/textures/DesertMap01.jpeg";
	unsigned char* data_map = stbi_load(file_map, &imWidth_map, &imHeight_map, &imNrChannels_map, 0);
	if (data_map)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imWidth_map, imHeight_map, 0, GL_RGB, GL_UNSIGNED_BYTE, data_map);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		std::cout << "Failed to Load texture" << std::endl;
		const char* reasonm = stbi_failure_reason();
		std::cout << reasonm << std::endl;
	}

	stbi_image_free(data_map);

	GLuint texture_sky;
	glGenTextures(1, &texture_sky);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_sky);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


	stbi_set_flip_vertically_on_load(true);
	int imWidth_sky, imHeight_sky, imNrChannels_sky;
	char file_sky[128] = "../../../../LAB03/textures/red_desert.jpeg";
	unsigned char* data_sky = stbi_load(file_sky, &imWidth_sky, &imHeight_sky, &imNrChannels_sky, 0);
	if (data_sky)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imWidth_sky, imHeight_sky, 0, GL_RGB, GL_UNSIGNED_BYTE, data_sky);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		std::cout << "Failed to Load texture" << std::endl;
		const char* reason_sky = stbi_failure_reason();
		std::cout << reason_sky << std::endl;
	}

	stbi_image_free(data_sky);



	double prev = 0;
	int deltaFrame = 0;

	glm::vec3 light_pos[] = { 
		glm::vec3(1.0, 2.0, 1.5),
		glm::vec3(-25.0, 15.0, 2.0),
		glm::vec3(2.0, 2.0, 1.0),
		glm::vec3(3.0,3.0,-4.0)
	};

	glm::vec3 light_colors[] = {
		glm::vec3(1.0, 0.0, 0.0),
		glm::vec3(100.0, 0.0, 20.0),
		glm::vec3(0.0, 0.5, 0.0),
		glm::vec3(0.0, 0.0, 1.0)
	};

	glm::mat4 player_model = glm::mat4(1.0);
	player_model = glm::translate(player_model, glm::vec3(2.0, 0.0, -2.0));
	player_model = glm::scale(player_model, glm::vec3(1.0, 1.0, 1.0));
	glm::mat4 player_inverseModel = glm::transpose(glm::inverse(player_model));

	glm::mat4 map_model = glm::mat4(1.0);
	map_model = glm::translate(map_model, glm::vec3(0.0, -2.0, 0.0));
	map_model = glm::scale(map_model, glm::vec3(5.0, 5.0, 5.0));
	glm::mat4 map_inverseModel = glm::transpose(glm::inverse(map_model));

	glm::vec3 l1Color = glm::vec3(1.0, 0.0, 0.0);
	light1.model = glm::translate(light1.model, glm::vec3(1.0, 2.0, 1.5));
	light1.model = glm::scale(light1.model, glm::vec3(0.1, 0.1, 0.1));
	glm::mat4 light_inverseModel = glm::transpose(glm::inverse(light1.model));

	glm::vec3 l2Color = glm::vec3(1.0, 0.0, 0.2);
	light2.model = glm::translate(light2.model, glm::vec3(-25.0, 15.0, 2.0));
	light2.model = glm::scale(light2.model, glm::vec3(1.0, 1.0, 1.0));

	glm::vec3 l3Color = glm::vec3(0.0, 0.5, 0.0);
	light3.model = glm::translate(light3.model, glm::vec3(2.0, 2.0, 1.0));
	light3.model = glm::scale(light3.model, glm::vec3(0.1, 0.1, 0.1));

	glm::vec3 l4Color = glm::vec3(0.0, 0.0, 1.0);
	light4.model = glm::translate(light4.model, glm::vec3(3.0, 3.0, -4.0));
	light4.model = glm::scale(light4.model, glm::vec3(0.1, 0.1, 0.1));

	sky.model = glm::translate(sky.model, glm::vec3(0.0, 0.0, 0.0));
	sky.model = glm::scale(sky.model, glm::vec3(20.0, 20.0, 20.0));
	


	glm::mat4 view = camera.GetViewMatrix();
	glm::mat4 perspective = camera.GetProjectionMatrix();

	float ambient = 0.1;
	float diffuse = 0.5;
	float specular = 0.5;

	glm::vec3 materialColour = glm::vec3(0.5, 0.5, 0.5);
	//Rendering

	player_shader.use();
	player_shader.setFloat("shininess", 32.0f);
	player_shader.setFloat("light.ambient_strength", ambient);
	player_shader.setFloat("light.diffuse_strength", diffuse);
	player_shader.setFloat("light.specular_strength", specular);
	player_shader.setFloat("light.constant", 0.5);
	player_shader.setFloat("light.linear", 0.15);
	player_shader.setFloat("light.quadratic", 0.03);

	map_shader.use();
	map_shader.setFloat("shininess", 1.0f);
	map_shader.setVector3f("materialColour", materialColour);
	map_shader.setFloat("light.ambient_strength", ambient);
	map_shader.setFloat("light.diffuse_strength", diffuse);
	map_shader.setFloat("light.specular_strength", specular);
	map_shader.setFloat("light.constant", 0.5);
	map_shader.setFloat("light.linear", 0.15);
	map_shader.setFloat("light.quadratic", 0.03);
	l_shader.use();
	sky_shader.use();

	while (!glfwWindowShouldClose(window)) {
		
		double now = glfwGetTime();
		double deltaTime = now - prev;
		deltaFrame++;
		if (deltaTime > 0.016) {
			processInput(window);
			prev = now;
			const double fpsCount = (double)deltaFrame / deltaTime;
			deltaFrame = 0;
			std::cout << "\r FPS: " << fpsCount;
			std::cout.flush();
		}
		
		view = camera.GetViewMatrix();
		glfwPollEvents();
		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		auto red_move = light_pos[0] + glm::vec3(0.0, 0.0, 4 * std::sin(now));
		auto green_move = light_pos[2] + glm::vec3(4 * std::sin(now), 0, -3 * std::sin(now/2));
		auto blue_move = light_pos[3] + glm::vec3(2 * std::sin(now), 2 * std::sin(now), 2 * std::sin(now));

		player_shader.use();
		player_shader.setVector3f("materialColour", materialColour);
		player_shader.setMatrix4("M", player_model);
		player_shader.setMatrix4("itM", player_inverseModel);
		player_shader.setMatrix4("V", view);
		player_shader.setMatrix4("P", perspective);
		player_shader.setVector3f("u_view_pos", camera.Position);
		player_shader.setVector3f("light.light_pos[0]", red_move);
		player_shader.setVector3f("light.light_pos[1]", light_pos[1]);
		player_shader.setVector3f("light.light_pos[2]", green_move);
		player_shader.setVector3f("light.light_pos[3]", blue_move);
		player_shader.setVector3f("light.light_color[0]", light_colors[0]);
		player_shader.setVector3f("light.light_color[1]", light_colors[1]);
		player_shader.setVector3f("light.light_color[2]", light_colors[2]);
		player_shader.setVector3f("light.light_color[3]", light_colors[3]);

		player.draw();
		
		l_shader.use();
		l_shader.setVector3f("materialColour", l1Color);
		glm::mat4 model1 = light1.model;
		model1 = glm::translate(model1, red_move);
		l_shader.setMatrix4("M",model1);
		l_shader.setMatrix4("itM", light_inverseModel);
		light_inverseModel = glm::transpose(glm::inverse(light2.model));
		l_shader.setMatrix4("V", view);
		l_shader.setMatrix4("P", perspective);
		light1.draw();

		l_shader.setVector3f("materialColour", l2Color);
		l_shader.setMatrix4("M", light2.model);
		light_inverseModel = glm::transpose(glm::inverse(light2.model));
		l_shader.setMatrix4("itM", light_inverseModel);
		l_shader.setMatrix4("V", view);
		l_shader.setMatrix4("P", perspective);
		light2.draw();

		l_shader.setVector3f("materialColour", l3Color);
		glm::mat4 model2 = light3.model;
		model2 = glm::translate(model2, green_move);
		l_shader.setMatrix4("M", model2);
		light_inverseModel = glm::transpose(glm::inverse(light3.model));
		l_shader.setMatrix4("itM", light_inverseModel);
		l_shader.setMatrix4("V", view);
		l_shader.setMatrix4("P", perspective);
		light3.draw();

		l_shader.setVector3f("materialColour", l4Color);
		glm::mat4 model3 = light4.model;
		model3 = glm::translate(model3, blue_move);
		l_shader.setMatrix4("M", model3);
		light_inverseModel = glm::transpose(glm::inverse(light4.model));
		l_shader.setMatrix4("itM", light_inverseModel);
		l_shader.setMatrix4("V", view);
		l_shader.setMatrix4("P", perspective);
		light4.draw();

		sky_shader.use();
		sky_shader.setInteger("ourTexture", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_sky);
		sky_shader.setMatrix4("M", sky.model);
		glm::mat4 sky_inverseModel = glm::transpose(glm::inverse(light4.model));
		sky_shader.setMatrix4("itM", sky_inverseModel);
		sky_shader.setMatrix4("V", view);
		sky_shader.setMatrix4("P", perspective);
		sky.draw();
		
		map_shader.use();
		map_shader.setInteger("ourTexture", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_map);
		map_shader.setMatrix4("M", map_model);
		map_shader.setMatrix4("itM", map_inverseModel);
		map_shader.setMatrix4("V", view);
		map_shader.setMatrix4("P", perspective);
		map_shader.setVector3f("u_view_pos", camera.Position);
		map_shader.setVector3f("light.light_pos[0]", red_move);
		map_shader.setVector3f("light.light_pos[1]", light_pos[1]);
		map_shader.setVector3f("light.light_pos[2]", green_move);
		map_shader.setVector3f("light.light_pos[3]", blue_move);
		map_shader.setVector3f("light.light_color[0]", light_colors[0]);
		map_shader.setVector3f("light.light_color[1]", light_colors[1]);
		map_shader.setVector3f("light.light_color[2]", light_colors[2]);
		map_shader.setVector3f("light.light_color[3]", light_colors[3]);
		plane.draw();

		
		glfwSwapBuffers(window);
	}

	//clean up ressource
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}


void processInput(GLFWwindow* window) {
	
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(LEFT, 0.1);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(RIGHT, 0.1);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(FORWARD, 0.1);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(BACKWARD, 0.1);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(1, 0.0, 1);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(-1, 0.0, 1);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, 1.0, 1);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, -1.0, 1);


}


