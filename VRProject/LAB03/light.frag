#version 330 core
out vec4 FragColor;
precision mediump float;
in vec3 v_frag_coord;
in vec3 v_normal;

uniform vec3 materialColour;

void main() {
	FragColor = vec4(materialColour.xyz, 1.0);
}