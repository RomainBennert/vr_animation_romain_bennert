#version 330 core
out vec4 FragColor;
precision mediump float;
in vec3 v_frag_coord;
in vec3 v_normal;
in vec2 v_tex_coord;
uniform sampler2D ourTexture;

void main() {
	vec4 Color = texture(ourTexture, v_tex_coord);
	FragColor = vec4(Color.xyz * vec3(0.5,0.3,0.4), 2.0);
}