# VR Project
This project was done by Romain Bennert.\
This project is set up with CMake. CMake should compile and allow the program to run if you set the debug target as ANIM_v001.exe
# File organisation
All extensions used are stored in the 3rdParty folder.\
The LAB03 Folders contains all the codefiles that handle the animation.\
animation_v001.cpp is the main c file. It's job is to call all other files and function for the animation. \
The object folder contains all .obj files while the texture folder contains all used textures (.jpeg files).\
camera.h, object.h and shader.h are c files to handle respectively the camera, 3D meshes and fragment and vertex shaders.